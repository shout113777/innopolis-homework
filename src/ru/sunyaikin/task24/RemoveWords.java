package ru.sunyaikin.task24;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class RemoveWords {
    public static void main(String[] args) {
        Set<String> words = new HashSet<>();
        words.add("foo");
        words.add("buzz");
        words.add("bar");
        words.add("fork");
        words.add("bort");
        words.add("spoon");
        words.add("!");
        words.add("dude");
        System.out.println(words);
        System.out.println(removeEvenLength(words));
    }

    /**
     * Удаляет элементы четной длины из Set
     *
     * @param set set
     * @return измененное множество без элементов четной длины
     */
    static Set<String> removeEvenLength(Set<String> set) {
        Iterator<String> Iter = set.iterator();
        while (Iter.hasNext()) {
            String word = Iter.next();
            if (word.length() % 2 == 0) {
                Iter.remove();
            }
        }
        return set;
    }
}
