package ru.sunyaikin.task8;

public class ExampleCounter {
   private static int counter=0;

    public ExampleCounter() {
        counter++;
    }

    public static int getCounter() {
        return counter;
    }

}
