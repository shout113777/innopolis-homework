package ru.sunyaikin.task8;

public class Calculator {
    public static void main(String[] args) {
        double a=4.0;
        double b=2.2;
        System.out.println(multiplication(a,b));
        int c=4;
        int d=2;
        System.out.println(multiplication(c,d));
    }

    private static int subtraction(int a, int b){
        return a-b;
    }
    private static double subtraction(double a, double b){
        return a-b;
    }

    private static int addition(int a, int b){
        return a+b;
    }
    private static double addition(double a, double b){
        return a+b;
    }
    private static int divide(int a, int b){
        return a/b;
    }
    private static double divide(double a, double b){
        return a/b;
    }
    private static int multiplication(int a, int b){
        return a*b;
    }
    private static double multiplication(double a, double b){
        return a*b;
    }
}
