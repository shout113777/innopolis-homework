package ru.sunyaikin.task8;

import java.util.Date;

public class TestConvert {
    public static void main(String[] args) {
        String[] goods = {"Кола, пепси, батон, пельмени"};
        Contract contract = new Contract(1, new Date(), goods);
        Act act = convert(contract);
        System.out.println(act);
    }

    private static Act convert(Contract contract) {
        int number = contract.getNumber();
        Date date = contract.getDate();
        String[] goods = contract.getGoods();
        return new Act(number, date, goods);
    }
}
