package ru.sunyaikin.task8;

import java.util.Date;

public class Contract {
    private int number;
    private Date date;
    private String[] goods;

    public Contract(int number, Date date, String[] goods) {
        this.number = number;
        this.date = date;
        this.goods = goods;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String[] getGoods() {
        return goods;
    }

    public void setGoods(String[] goods) {
        this.goods = goods;
    }
}
