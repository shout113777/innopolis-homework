package ru.sunyaikin.task18;

import java.io.*;
import java.util.Scanner;

public class Convertor {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите кодировку исходного файла");
        String encodingRead = scan.nextLine();
        System.out.println("Введите кодировку будущего файла");
        String encodingWrite = scan.nextLine();
        //Чтение из потока байт
        try (InputStream is = new FileInputStream(encodingRead + ".txt")) {
            byte[] buf = new byte[is.available()];
            int b;
            if ((b = is.read(buf)) != -1) {
                String s = new String(buf, encodingRead);
                System.out.println(s);
                try (OutputStream os = new FileOutputStream(encodingWrite + ".txt")) {
                    os.write(s.getBytes(encodingWrite));
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Назовите исходный файл в формате: название кодировки.txt");
        }
    }
}
