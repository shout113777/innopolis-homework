package ru.sunyaikin.task3;

import java.util.Scanner;

public class MinimumNumber {
    public static void main(String[] args) {
        System.out.print("Введите 2 числа через Enter: ");
        Scanner scanNumberOne = new Scanner(System.in);

        Scanner scanNumberTwo = new Scanner(System.in);
        if (scanNumberOne.hasNextInt() & scanNumberTwo.hasNextInt()){
            findMinimumNumber(scanNumberOne.nextInt(),scanNumberTwo.nextInt());
        }

    }
    private static void findMinimumNumber(int a, int b){
        if(a<b){
            System.out.println(a + " является минимальным числом");
        }else if(b<a){
            System.out.println(b + " является минимальным числом");
        }else{
            System.out.println("Числа равны");
        }
    }
}
