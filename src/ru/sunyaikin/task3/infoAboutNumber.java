package ru.sunyaikin.task3;

import java.util.Scanner;

public class infoAboutNumber {
    public static void main(String[] args) {
        System.out.print("Введите число: ");
        Scanner scan = new Scanner(System.in);
        if (scan.hasNextInt()) {
            int number = scan.nextInt();
            getInfoAboutSignNumber(number);
            getInfoAboutEvenParity(number);

        } else {
            System.out.println("Введите число!");
        }

    }

    /**
     * Функция для получения информации о том, положительное число, отрицательное или равно 0
     *
     * @param number
     */
    private static void getInfoAboutSignNumber(int number) {
        if (number > 0) {
            System.out.println("Число является положительным");
        } else if (number < 0) {
            System.out.println("Число является отрицательным");
        } else {
            System.out.println("Число равно 0");
        }
    }

    /**
     * Функция проверки числа на четность
     *
     * @param number
     */
    private static void getInfoAboutEvenParity(int number) {
        if (number % 2 == 0) {
            System.out.println("Число является четным");
        } else {
            System.out.println("Число является нечетным");
        }
    }
}
