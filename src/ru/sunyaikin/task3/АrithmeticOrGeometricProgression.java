package ru.sunyaikin.task3;

import java.util.Scanner;

public class АrithmeticOrGeometricProgression {
    public static void main(String[] args) {
        System.out.println("Меню");
        System.out.println("1. Вычислить арифметическую прогрессию");
        System.out.println("2. Вычислить геометрическую прогрессию");
        System.out.println("3. Вычислить обе прогрессии");
        Scanner scanNumberMenu = new Scanner(System.in);

        if(scanNumberMenu.hasNextInt()){
            int numberMenu=scanNumberMenu.nextInt();
            System.out.println("Введите a");
            Scanner scanA = new Scanner(System.in);
            System.out.println("Введите n");
            Scanner scanN = new Scanner(System.in);
            System.out.println("Введите d/q");
            Scanner scanD = new Scanner(System.in);
            if(numberMenu==1){
                if(scanA.hasNextInt() & scanN.hasNextInt() & scanD.hasNextInt()){
                    int a = scanA.nextInt();
                    int n = scanN.nextInt();
                    int d = scanD.nextInt();
                    getАrithmeticProgression(a,n,d);
                }
            }else if(numberMenu==2){
                if(scanA.hasNextInt() & scanN.hasNextInt() & scanD.hasNextInt()){
                    int a = scanA.nextInt();
                    int n = scanN.nextInt();
                    int d = scanD.nextInt();
                    getGeometricProgression(a,n,d);
                }
            }else{
                if(scanA.hasNextInt() & scanN.hasNextInt() & scanD.hasNextInt()){
                    int a = scanA.nextInt();
                    int n = scanN.nextInt();
                    int d = scanD.nextInt();
                    getАrithmeticProgression(a,n,d);
                    System.out.println();
                    getGeometricProgression(a,n,d);
                }

            }
        }else{
            System.out.println("Выберите пункт меню");
        }
    }
    private static void getАrithmeticProgression(int a,int n,double d){
        System.out.println("Арифметическая последовательность: ");
        for(int i=1;i<=n;i++){
            System.out.print(a+(i-1)*d+" ");
        }
    }
    private static void getGeometricProgression(int b,int n,double q){
        System.out.println("Геометрическая последовательность: ");
        for(int i=1;i<=n;i++){
            System.out.print(b*Math.pow(q,i-1)+" ");
        }
    }
}
