package ru.sunyaikin.task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Salary {
    public static void main(String[] args) {
        double salary;
        System.out.print("Введите вашу \"черную\" зарплату: ");
        Scanner scan = new Scanner(System.in);
        try {
            salary = scan.nextDouble();
        }catch (InputMismatchException e){
            System.out.println("Ошибка ввода. Попробуйте еще раз");
            return;
        }
        calculateWhiteSalary(salary);
    }

    /**
     * Функция для подсчета зарплаты после вычета НДФЛ
     *
     * @param salary
     */
    private static void calculateWhiteSalary(double salary) {
        System.out.println("Ваша зарплата после вычета НДФЛ: " + salary * 0.87);
    }
}
