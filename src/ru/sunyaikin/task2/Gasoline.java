package ru.sunyaikin.task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Gasoline {
    public static void main(String[] args) {

        double cost,countGasoline;
        System.out.print("Введите стоимость бензина: ");
        Scanner scanCost = new Scanner(System.in);
        try {
            cost = scanCost.nextDouble();
        }catch (InputMismatchException e){
            System.out.println("Ошибка ввода. Попробуйте еще раз");
            return;
        }
        System.out.print("Введите количество литров бензина: ");
        Scanner scanCountGasoline = new Scanner(System.in);
        try {
            countGasoline=scanCountGasoline.nextDouble();
        }catch (InputMismatchException e){
            System.out.println("Ошибка ввода. Попробуйте еще раз");
            return;
        }
        calculateCostGasoline(cost,countGasoline);
    }
    /**
     * Функция для подсчета стоимости бензина
     * @param cost
     * @param countGasoline
     */
    private static void calculateCostGasoline(double cost, double countGasoline) {
            System.out.println("Стоимость бензина будет составлять "+ cost*countGasoline+" рублей");
    }
}
