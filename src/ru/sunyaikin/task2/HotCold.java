package ru.sunyaikin.task2;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;


public class HotCold {
    static int countAttempts = 0;

    public static void main(String[] args) {
        boolean answerFound = false;
        Random r = new Random();
        int randomNumber = 1 + r.nextInt(99);
        Scanner scan = new Scanner(System.in);
        if(countAttempts>1){
            System.out.println("Если вы хотите выйти из игры, введите буквы или enter");
        }else {
            System.out.print("Введите число: ");
        }

        while (!answerFound & scan.hasNextInt()) {
            countAttempts++;
            answerFound = checkAnswer(randomNumber, scan.nextInt());
            System.out.print("Введите число: ");
        }

        if (!answerFound) {
            System.out.println("Вы проиграли");
        } else {
            System.out.println("Поздравляю!");
            System.out.println("Вы угадали число!");
        }
        System.out.println("Количество попыток: " + countAttempts);
    }

    private static boolean checkAnswer(int randomNumber, int number) {
        int distance = Math.abs(randomNumber - number);
        if (number == randomNumber) {
            return true;
        }
        System.out.println();
        if (distance >= 60) {
            System.out.println("Очень холодно");
        } else if (distance >= 40) {
            System.out.println("Холодно");
        } else if (distance >= 30) {
            System.out.println("Теплее");
        } else if (distance >= 15) {
            System.out.println("Тепло");
        } else if (distance >= 5) {
            System.out.println("Очень тепло");
        } else if (distance >= 3) {
            System.out.println("Горячо");
        } else if (distance >= 1) {
            System.out.println("Очень горячо!!!");
        }
        return false;
    }
}