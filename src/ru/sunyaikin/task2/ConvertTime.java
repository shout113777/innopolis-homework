package ru.sunyaikin.task2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ConvertTime {
    public static void main(String[] args) {
        int seconds;
        System.out.print("Введите количество секунд: ");
        Scanner scan = new Scanner(System.in);
        try {
            seconds = scan.nextInt();
        }catch (InputMismatchException e) {
            System.out.println("Ошибка ввода. Попробуйте еще раз");
            return;
        }
        convertAndPrintSecondsToHours(seconds);
    }
    /**
     * Функция конвертирует секунды в часы
     * @param seconds
     */
    private static void convertAndPrintSecondsToHours(int seconds){
        int hours= seconds/3600;
        System.out.println("Количество часов: "+hours);
    }
}