package ru.sunyaikin.task19;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;

public class CashVoucher {
    public static void main(String[] args) {
        Formatter f = new Formatter();
        int i = 0;
        int j = 0;
        BigDecimal sum = BigDecimal.ZERO.setScale(2);

        //Запись в лист данных из файла

        List<List<String>> list = new ArrayList<>();
        list.add(new ArrayList<String>());
        try {
            FileInputStream fstream = new FileInputStream("products.txt");
            BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                if (i != 3) {
                    list.get(j).add(strLine);
                } else {
                    list.add(new ArrayList<String>());
                    i = 0;
                    j++;
                    list.get(j).add(strLine);
                }
                i++;
                System.out.println(strLine);
            }
        } catch (IOException e) {
            System.out.println("Ошибка");
        }

        //Вывод чека
        try (PrintWriter out = new PrintWriter("out.txt")) {

            f = new Formatter();
            f.format("%-12.16s %13.13s %7.8s %.10s", "Наименование", "Цена   ", "Кол-во   ", "Стоимость ");
            //System.out.println(f);
            out.println(f);
            f = new Formatter();
            f.format("%.46s", "================================================");
            //System.out.println(f);
            out.println(f);
            for (i = 0; i < list.size(); i++) {
                String productName = list.get(i).get(0);
                BigDecimal quintity = new BigDecimal(list.get(i).get(1));
                BigDecimal price = new BigDecimal(list.get(i).get(2));
                printBeatyVoucherLine(productName, price, quintity, out);
                sum = sum.add(price.multiply(quintity).setScale(2, RoundingMode.CEILING));
            }
            f = new Formatter();
            f.format("%.46s", "================================================");
            //System.out.println(f);
            out.println(f);
            f = new Formatter();
            f.format("%-7.7s %38.2f", "Итого:", sum);
            //System.out.println(f);
            out.println(f);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
//        String test = "4.5";
//        int test1 = Integer.parseInt(test);
//        System.out.println(test1);
    }

    private static void printBeatyVoucherLine(String productName, BigDecimal price, BigDecimal quintity, PrintWriter out) {
        Formatter f = new Formatter();
        f.format("%-15.16s %7.2f х %7.3f =%11.2f", productName, price, quintity, price.multiply(quintity).setScale(2, RoundingMode.CEILING));
        //System.out.println(f);
        out.println(f);
    }

}
