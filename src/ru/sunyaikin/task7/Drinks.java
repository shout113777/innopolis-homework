package ru.sunyaikin.task7;

public enum Drinks {
    COLA(30, 3, "Coca-Cola"), PEPSI(35, 1, "Pepsi");

    private Integer price;
    private Integer count;
    private String label;
    Drinks(Integer price, Integer count, String label){
        this.price = price;
        this.count = count;
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "Drinks{" +
                "price=" + price +
                ", count=" + count +
                '}';
    }


}
