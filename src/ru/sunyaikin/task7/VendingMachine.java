package ru.sunyaikin.task7;


import org.apache.log4j.Logger;

import java.util.Scanner;


public class VendingMachine {
    private static final Logger logger = Logger.getLogger(VendingMachine.class.getName());
    //private static final Logger logger = Logger
    private static int money = 0;
    private static Scanner scan = new Scanner(System.in);
    public static void main(String[] args) {
        logger.info("Вендинговый автомат запущен");

        boolean end = false;
        addMoney();
        logger.info("Запущена функция addMoney()");

        do {
            System.out.println("Выберите , что вы хотите сделать:");
            System.out.println("1. Добавить купюры");
            System.out.println("2. Выбрать напиток");
            System.out.println("3. Завершить обслуживание");
            //if (scan.hasNextInt()) {
                int menuItem = scan.nextInt();
                switch (menuItem) {
                    case 1:
                        addMoney();
                        break;
                    case 2:
                        chooseDrink();
                        break;
                    case 3:
                        end = true;
                        break;
                    default:
                        System.out.println("Введите цифру пункта меню");
                }
//            } else {
//                System.out.println("Введите цифру пункта меню");
//            }
        } while (!end);
        if(money!=0){
            System.out.println("Ваша сдача: "+money);
            money=0;
        }
    }

    private static void chooseDrink() {
        logger.info("Запущена функция chooseDrink()");
        Drinks[] drinks = Drinks.values();
        System.out.println("Выберите напиток");
        int countDrinks=0;
        for(int i = 0; i< drinks.length; i++){

            if(drinks[i].getCount()>0){
                countDrinks++;
                System.out.printf("%d. %s - %d рублей\n",i+1,drinks[i].getLabel(),drinks[i].getPrice());
            }
        }
        if(countDrinks==0){
            logger.warn("Закончились напитки");
            System.out.println("Напитки закончились");
        }else{
            int idChoiceCustomer=scan.nextInt()-1;
            buyDrink(idChoiceCustomer,drinks);
        }

    }

    private static void buyDrink(int idChoiceCustomer,Drinks[] drinks) {
        logger.info("Запущена функция buyDrink()");
        int count=drinks[idChoiceCustomer].getCount();
        int cost=drinks[idChoiceCustomer].getPrice();
        if(money-cost<0){
            logger.warn("Недостаточно денег");
            System.out.println("У вас недостаточно денег");
        }else{
            if(count!=0) {
                drinks[idChoiceCustomer].setCount(count - 1);
                money -= cost;
                System.out.println("Ваш баланс: " + money);
            }
        }


    }

    private static void addMoney() {
        logger.info("Запущена функция addMoney()");
        System.out.println("Вставьте купюры:");
        int addMoneyToBalance=scan.nextInt();
        if(addMoneyToBalance<=0){
            logger.warn("Ошибка при пополнении баланса");
            System.out.println("Ошибка ввода денег.Пожалуйста пополните баланс");
        }else{
            money +=addMoneyToBalance;
        }

        System.out.println("Баланс: " + money);
    }


}
