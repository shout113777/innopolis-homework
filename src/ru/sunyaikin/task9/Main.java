package ru.sunyaikin.task9;

public class Main {
    public static void main(String[] args) {
        Tiger tiger = new Tiger();
        System.out.println(tiger.getName());
        tiger.swim();

        Man student = new Man() {
        };
        student.run();
    }
}
