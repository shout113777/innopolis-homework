package ru.sunyaikin.task9;

public class Cat extends Animal implements Run {
    @Override
    public String getName() {
        return "Я кот";
    }

    @Override
    public void run() {
        System.out.println("Я плыву");
    }
}
