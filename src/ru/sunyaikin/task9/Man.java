package ru.sunyaikin.task9;

abstract class Man implements Run, Swim {

    @Override
    public void run() {
        System.out.println("Я могу бегать");
    }

    @Override
    public void swim() {
        System.out.println("Я могу плавать");
    }
}
