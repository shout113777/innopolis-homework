package ru.sunyaikin.task9;

public class Duck extends Animal implements Swim, Fly, Run {
    @Override
    public String getName() {
        return "Я утка";
    }

    @Override
    public void swim() {
        System.out.println("Я плаваю");
    }

    @Override
    public void fly() {
        System.out.println("Я летаю");
    }

    @Override
    public void run() {
        System.out.println("Я бегаю");
    }
}
