package ru.sunyaikin.task9;

public class Tiger extends Animal implements Swim, Run {
    @Override
    public String getName() {
        return "Я тигр";
    }

    @Override
    public void swim() {
        System.out.println("Я плаваю");
    }

    @Override
    public void run() {
        System.out.println("Я плаваю");
    }
}
