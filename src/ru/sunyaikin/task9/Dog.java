package ru.sunyaikin.task9;

public class Dog extends Animal implements Run, Swim {
    @Override
    public String getName() {
        return "Я собака";
    }

    @Override
    public void run() {
        System.out.println("Я бегаю");
    }

    @Override
    public void swim() {
        System.out.println("Я плаваю");
    }
}
