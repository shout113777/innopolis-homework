package ru.sunyaikin.task25;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Shop implements Basket {
    Map<String, Integer> map = new HashMap<>();

    public static void main(String[] args) {

    }

    /**
     * Добавляет товар и его количество в корзину
     *
     * @param product  название товара
     * @param quantity количество
     */
    @Override
    public void addProduct(String product, int quantity) {
        if (map.containsKey(product)) {
            map.put(product, map.get(product) + quantity);
        } else {
            map.put(product, quantity);
        }
    }

    /**
     * Удаляет товар из корзины
     *
     * @param product название продукта
     */
    @Override
    public void removeProduct(String product) {
        map.remove(product);
    }

    /**
     * Обновляет количество товар в корзине
     *
     * @param product  название товара
     * @param quantity количество товара
     */
    @Override
    public void updateProductQuantity(String product, int quantity) {
        map.put(product, quantity);
    }

    /**
     * Очищает корзину
     */
    @Override
    public void clear() {
        map.clear();
    }

    /**
     * @return возвращает список товаров в корзине
     */
    @Override
    public Set<String> getProducts() {
        return map.keySet();
    }

    /**
     * @param product
     * @return возвращает количество товара
     */
    @Override
    public int getProductQuantity(String product) {
        if (map.containsKey(product)) {
            return map.get(product);
        } else {
            System.out.println("Ошибка.Попробуйте ввести правильное значение");
            return -1;
        }
    }
}
