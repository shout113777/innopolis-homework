package ru.sunyaikin.task25;

import java.util.HashMap;
import java.util.Map;

public class Filter {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("Вася", "Иванов");
        map.put("Петр", "Петров");
        map.put("Виктор", "Сидоров");
        map.put("Сергей", "Савельев");
        map.put("Вадим", "Сергеев");
        System.out.println(map);
        System.out.println(isUnique(map));
    }

    public static boolean isUnique(Map<String, String> map) {
        Map<String, Integer> mapValues = new HashMap<>();
        for (String key : map.values()) {
            if (mapValues.containsKey(key)) {
                return false;
            } else {
                mapValues.put(key, 0);
            }
        }
        return true;
    }

    ;
}
