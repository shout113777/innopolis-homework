package ru.sunyaikin.task25;


public class App {
    public static void main(String[] args) {
        Shop shop = new Shop();
        shop.addProduct("Телефон", 1);
        shop.addProduct("Телефон", 2);
        shop.addProduct("Телефон", 1);
        shop.addProduct("Машина", 2);
        System.out.println(shop.getProducts());
        shop.updateProductQuantity("Машина", 10);
        System.out.println(shop.getProductQuantity("Телефон"));
        System.out.println(shop.getProductQuantity("Машина"));
        shop.removeProduct("Машина");
        System.out.println(shop.getProducts());
        shop.clear();
        System.out.println(shop.getProducts());
    }
}
