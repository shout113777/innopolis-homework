package ru.sunyaikin.task25;

import java.util.Set;

public interface Basket {
    void addProduct(String product, int quantity);

    void removeProduct(String product);

    void updateProductQuantity(String product, int quantity);

    void clear();

    Set<String> getProducts();

    int getProductQuantity(String product);
}
