package ru.sunyaikin.task1;

public class ConvertTime {
    public static void main(String[] args) {
        int seconds=7300;
        convertAndPrintSecondsToHours(seconds);
    }
    /**
     * Функция конвертирует секунды в часы
     * @param seconds
     */
    private static void convertAndPrintSecondsToHours(int seconds){
        int hours= seconds/60/60;
        System.out.println("Количество часов: "+hours);
    }
}