package ru.sunyaikin.task1;

public class Salary {
    public static void main(String[] args) {
        double salary = 20000;
        calculateWhiteSalary(salary);
    }

    /**
     * Функция для подсчета зарплаты после вычета НДФЛ
     *
     * @param salary
     */
    private static void calculateWhiteSalary(double salary) {
        System.out.println("Ваша зарплата после вычета НДФЛ: " + salary * 0.87);
    }
}
