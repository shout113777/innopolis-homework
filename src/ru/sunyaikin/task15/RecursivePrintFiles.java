package ru.sunyaikin.task15;

import java.io.File;

public class RecursivePrintFiles {
    public static void main(String[] args) {
        File dir = new File("dir");

        printfile(dir, 0);
    }


    private static void printfile(File dir, int depth) {
        String text = "";
        for (int i = 0; i < depth; i++) {
            text += " ";
        }
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isDirectory()) {
                System.out.println(text + file.getName());
                printfile(file, depth + 1);
            } else {
                System.out.println(text + file.getName());
            }


        }
    }
}
