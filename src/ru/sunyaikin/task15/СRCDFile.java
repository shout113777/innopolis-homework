package ru.sunyaikin.task15;

import java.io.*;

public class СRCDFile {
    /**
     * Create, Rename,Copy and Delete file
     */
    public static void main(String[] args) throws IOException {
        String nameFile = "file.txt";
        File file = new File(nameFile);
        file.createNewFile();
        file.renameTo(new File("newFile.txt"));
        InputStream is = new FileInputStream("newFile.txt");
        OutputStream os = new FileOutputStream("copynewfile.txt");
        try {
            int b;
            while ((b = is.read()) != -1) {
                os.write(b);
            }
        } finally {
            is.close();
            os.close();
        }
        file.delete();
    }


}
