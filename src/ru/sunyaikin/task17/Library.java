package ru.sunyaikin.task17;

import java.io.*;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Library {
    private static final String filename = "Book.bin";


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        ArrayList<Book> listBook;
        //Восстановление содержимого после перезапуска
        try {
            listBook = load();
        } catch (FileNotFoundException e) {
            System.out.println("Библиотека пуста. Пожалуйста добавьте книги");
            listBook = new ArrayList<Book>();
        }

        Scanner scan = new Scanner(System.in);
        System.out.println("Если хотите выйти, напишите любые слова, буквы, символы ");
        System.out.println("Выберите , что вы хотите сделать:");
        System.out.println("1. Добавить книгу");
        System.out.println("2. Вывести список всех книг");
        System.out.print("Введите число: ");
        while (scan.hasNextInt()) {
            int menuItem = scan.nextInt();
            switch (menuItem) {
                case 1:
                    try {
                        addBook(listBook);
                    } catch (InputMismatchException e) {
                        System.out.println("Введите число");
                    }
                    break;
                case 2:
                    showList(listBook);
                    break;
                default:
                    System.out.println("Введите цифру пункта меню");
            }
            System.out.println("Если хотите выйти, напишите любые слова, буквы, символы ");
            System.out.println("Выберите , что вы хотите сделать:");
            System.out.println("1. Добавить книгу");
            System.out.println("2. Вывести список всех книг");
            System.out.print("Введите число: ");
        }
        save(listBook);

//        add("Новая книга","Суняйкин С.А",2015,listBook);
//        add("Новая книга","Суняйкин С.А",2014,listBook);

//        System.out.println("Был: "+ Arrays.toString(listBook.toArray()));


    }

    private static void showList(ArrayList<Book> listBooks) {
        System.out.println("Список книг:");
        for (int i = 0; i < listBooks.size(); i++) {
            System.out.println(listBooks.get(i));
        }

    }

    private static void addBook(ArrayList<Book> listBooks) {
        System.out.println("Введите назвние книги");
        Scanner scan = new Scanner(System.in);
        String name = scan.nextLine();
        System.out.println("Введите автора книги");
        String author = scan.nextLine();
        System.out.println("Введите год издания книги");
        int year = scan.nextInt();
        add(name, author, year, listBooks);
    }


    public static ArrayList<Book> load() throws IOException, ClassNotFoundException {
        try (ObjectInputStream ois = new ObjectInputStream(
                new FileInputStream(filename)
        )) {
            return (ArrayList<Book>) ois.readObject();
        }

    }

    private static ArrayList<Book> add(String name, String author, int year, ArrayList<Book> listBooks) {
        Book book = new Book(name, author, year);
        listBooks.add(book);
        return listBooks;
    }

    private static void save(ArrayList<Book> listBook) {
        try (ObjectOutputStream oos = new ObjectOutputStream(
                new FileOutputStream(filename)
        )) {
            oos.writeObject(listBook);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
