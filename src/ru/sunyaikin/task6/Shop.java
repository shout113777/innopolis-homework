package ru.sunyaikin.task6;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Shop {
    private BigDecimal money;
    private List<Person> personal ;
    private Person admin ;



    public List<Person> getPersonal() {
        return personal;
    }

    public void setPersonal(List<Person> personal) {
        this.personal = personal;
    }

    public void addPerson(Person p){
        personal.add(p);
    }

    public void printPersonal(){
        for (Person p: personal){
            System.out.println(p);
        }
    }

    public Person getAdmin() {
        return admin;
    }

    public void setAdmin(Person admin) {
        this.admin = admin;
    }

    public  void  countMoney (){
        System.out.println(admin.getFirstName()+ " считает деньги");
    }

    public Shop(BigDecimal money, List<Person> personal, Administrator admin) {
        this.money = money;
        this.personal = personal;
        this.admin = admin;
    }
}
