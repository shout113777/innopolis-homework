package ru.sunyaikin.task6;

import java.time.LocalDate;

public class Administrator extends Person{
    public boolean accessRegister=true;
    public Administrator(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    public boolean isAccessRegister() {
        return accessRegister;
    }

    public void setAccessRegister(boolean accessRegister) {
        this.accessRegister = accessRegister;
    }

    @Override
    public String toString() {
        return "Administrator{" +
                "accessRegister=" + accessRegister +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", birthday=" + birthday +
                '}';
    }
}
