package ru.sunyaikin.task6;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;

public class Programmer {
    public static void main(String[] args) {
       Administrator adminShop = new Administrator("Алексей","Иванов", LocalDate.of(1998,12,12));
        Shop ns = new Shop(BigDecimal.valueOf(30000), new ArrayList<>(),adminShop);
        ns.countMoney();
        ns.addPerson(new Worker("Сергей","Иванов", LocalDate.of(1996,10,12)));
        ns.addPerson(new Worker("Инна","Иванова", LocalDate.of(1996,10,12)));
        ns.addPerson(new Worker("Инна","Иванова", LocalDate.of(1996,10,12)));
        ns.addPerson(adminShop);
        ns.printPersonal();
        ns.setAdmin(ns.getPersonal().get(0));
        ns.countMoney();
    }
}
