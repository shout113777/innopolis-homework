package ru.sunyaikin.task6;

import java.time.LocalDate;

public class Worker extends Person{
    public static boolean accessTill=false;
    public Worker(String firstName, String lastName, LocalDate birthday) {
        super(firstName, lastName, birthday);
    }

    public static boolean isAccessTill() {
        return accessTill;
    }

    public static void setAccessTill(boolean accessTill) {
        Worker.accessTill = accessTill;
    }

}
