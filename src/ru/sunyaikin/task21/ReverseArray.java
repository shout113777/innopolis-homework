package ru.sunyaikin.task21;

public class ReverseArray {
    public static void main(String[] args) {
        int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        array = reverseArray(array);

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }

    }

    private static int[] reverseArray(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int swap = array[array.length - i - 1];
            array[array.length - i - 1] = array[i];
            array[i] = swap;
        }
        return array;
    }
}
