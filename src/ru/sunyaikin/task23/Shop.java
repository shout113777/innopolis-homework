package ru.sunyaikin.task23;

import java.util.ArrayList;
import java.util.List;

public class Shop implements Basket {
    List<String> products = new ArrayList<>();
    List<Integer> count = new ArrayList<>();

    public static void main(String[] args) {

    }

    /**
     * Добавляет товар и его количество в корзину
     *
     * @param product  название товара
     * @param quantity количество
     */
    @Override
    public void addProduct(String product, int quantity) {
        if (products.contains(product)) {
            int indexProduct = products.indexOf(product);
            Integer v = count.get(indexProduct);
            v++;
            count.set(indexProduct, v);
            System.out.println(indexProduct);
        } else {
            products.add(product);
            count.add(quantity);
        }
    }

    /**
     * Удаляет товар из корзины
     * @param product название продукта
     */
    @Override
    public void removeProduct(String product) {
        try {
            int indexProduct = products.indexOf(product);
            products.remove(indexProduct);
            count.remove(indexProduct);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Введите товар, находящийся в корзине");
        }

    }

    /**
     * Обновляет количество товар в корзине
     * @param product название товара
     * @param quantity количество товара
     */
    @Override
    public void updateProductQuantity(String product, int quantity) {
        try {
            int indexProduct = products.indexOf(product);
            count.set(indexProduct, quantity);
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Введите товар, находящийся в корзине");
        }

    }

    /**
     * Очищает корзину
     */
    @Override
    public void clear() {
        products.clear();
        count.clear();
    }

    /**
     *
     * @return возвращает список товаров в корзине
     */
    @Override
    public List<String> getProducts() {
        return products;
    }

    /**
     *
     * @param product
     * @return возвращает количество товара
     */
    @Override
    public int getProductQuantity(String product) {
        try {
            int indexProduct = products.indexOf(product);
            Integer v = count.get(indexProduct);
            return v;
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Введите товар, находящийся в корзине");
            return -1;
        }

    }
}
