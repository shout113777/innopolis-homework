package ru.sunyaikin.task20;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class BitcoinStats {
    public static void main(String[] args) throws MalformedURLException {
        URL url = new URL("https://api.blockchair.com/bitcoin/stats");
        //String  json="{\"data\":{\"blocks\":\"584144\",\"transactions\":432185289,\"outputs\":1159405348,\"circulation\":1780177104497237,\"blocks_24h\":182,\"transactions_24h\":366575,\"difficulty\":7934713219630.6,\"volume_24h\":96186320399416,\"mempool_transactions\":1918,\"mempool_size\":3159317,\"mempool_tps\":5.8,\"mempool_total_fee_usd\":2411.8381,\"best_block_height\":584143,\"best_block_hash\":\"00000000000000000022584e85c11b89678c9ef57858c0ba0ade809d9ca3f71d\",\"best_block_time\":\"2019-07-06 10:06:52\",\"blockchain_size\":228805837815,\"average_transaction_fee_24h\":18495,\"inflation_24h\":227500000000,\"median_transaction_fee_24h\":10000,\"cdd_24h\":8534501.196833508,\"largest_transaction_24h\":{\"hash\":\"73573b8ab1e0f95811c9db6698f4636efa6982724e99643d68936a4fcb7b5c1a\",\"value_usd\":72341336},\"nodes\":9368,\"hashrate_24h\":\"71897328652847500465\",\"inflation_usd_24h\":25975950,\"average_transaction_fee_usd_24h\":2.111775158864516,\"median_transaction_fee_usd_24h\":1.1418000000000001,\"market_price_usd\":11418,\"market_price_btc\":1,\"market_price_usd_change_24h_percentage\":1.66322,\"market_cap_usd\":203419252576,\"market_dominance_percentage\":62.51,\"next_retarget_time_estimate\":\"2019-07-09 11:03:24\",\"next_difficulty_estimate\":8406124072348,\"countdowns\":[{\"event\":\"Reward halving\",\"time_left\":27513000}],\"suggested_transaction_fee_per_byte_sat\":2},\"context\":{\"code\":200,\"source\":\"A\",\"time\":0.2179088592529297,\"state\":584143,\"cache\":{\"live\":false,\"duration\":\"Ignore\",\"since\":\"2019-07-06 10:11:31\",\"until\":\"2020-07-05 10:11:31\",\"time\":3.814697265625e-6},\"api\":{\"version\":\"2.0.31\",\"last_major_update\":\"2018-07-19 18:07:19\",\"next_major_update\":\"2019-07-19 18:07:19\",\"tested_features\":\"omni-v.a1,whc-v.a1,aggregate-v.b5,xpub-v.b5,ripple-v.a1,ethgraph-v.a1\",\"documentation\":\"https:\\/\\/github.com\\/Blockchair\\/Blockchair.Support\\/blob\\/master\\/API.md\"}}}";
        try (InputStream is = url.openStream();
             Reader reader = new InputStreamReader(is);
             BufferedReader br = new BufferedReader(reader);
        ) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, false);

            Data btc = objectMapper.readValue(br.readLine(), Data.class);
            System.out.println(btc.getData());
            // System.out.println(br.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
