package ru.sunyaikin.task20;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Bitcoin {
    private long blocks;
    private long transactions;
    private long difficulty;

    public Bitcoin() {

    }

    public long getBlocks() {
        return blocks;
    }

    public void setBlocks(long blocks) {
        this.blocks = blocks;
    }

    public long getTransactions() {
        return transactions;
    }

    public void setTransactions(long transactions) {
        this.transactions = transactions;
    }

    public long getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(long difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public String toString() {
        return "Bitcoin{" +
                "blocks=" + blocks +
                ", transactions=" + transactions +
                ", difficulty=" + difficulty +
                '}';
    }
}
