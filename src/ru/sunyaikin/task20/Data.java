package ru.sunyaikin.task20;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Data {
    private Bitcoin data;

    public Bitcoin getData() {
        return data;
    }

    public void setData(Bitcoin data) {
        this.data = data;
    }
}
