package ru.sunyaikin.task13;

public enum Eats {
    PORRIDGE("Каша"), CARROT("Морковь");

    private String label;

    Eats(String label) {

        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }


    @Override
    public String toString() {
        return "Eats{" +
                "label='" + label + '\'' +
                '}';
    }


}
