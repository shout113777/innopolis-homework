package ru.sunyaikin.task22;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person(31, "Alex"));
        people.add(new Person(30, "Alex"));
        people.add(new Person(21, "Serhio"));
        people.add(new Person(22, "Alena"));
        Collections.sort(people, new SortedByNameAndAge());
        //Collections.sort(people, Comparator.comparing(Person::getName).thenComparing(Person::getAge));

        iterate(people);
    }

    private static void iterate(List<Person> people) {
        Iterator i = people.iterator();
        while (i.hasNext()) {
            System.out.print(i.next() + " : ");
        }
    }
}
