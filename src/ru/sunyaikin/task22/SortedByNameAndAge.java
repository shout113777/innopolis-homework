package ru.sunyaikin.task22;

import java.util.Comparator;

public class SortedByNameAndAge implements Comparator<Person> {
    public int compare(Person obj1, Person obj2) {
        String str1 = obj1.getName();
        String str2 = obj2.getName();
        int age1 = obj1.getAge();
        int age2 = obj2.getAge();
        if (str1.equals(str2)) {
            if (age1 == age2) {
                return 0;
            } else if (age1 > age2) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return obj1.getName().compareTo(obj2.getName());

        }

    }

}
